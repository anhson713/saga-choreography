package org.trainingjava.orderclient.event;

public enum OrderStatus {

    SUCCESS,
    IN_PROGRESS,
    FAILED,
    CANCELED
}

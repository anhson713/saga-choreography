package org.trainingjava.orderclient.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderPaymentEvent {

    private String eventId;
    private String orderId;
    private OrderStatus orderStatus;
    private String userId;
    private Long amount;
}

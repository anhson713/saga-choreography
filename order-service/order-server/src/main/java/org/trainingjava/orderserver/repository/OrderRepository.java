package org.trainingjava.orderserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.trainingjava.orderserver.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, String> {
}

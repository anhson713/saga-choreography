package org.trainingjava.orderserver.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.util.Map;

public record OrderRequest(
        @NotBlank
        String userId,
        @NotEmpty
        Map<String, Long> items) {

}

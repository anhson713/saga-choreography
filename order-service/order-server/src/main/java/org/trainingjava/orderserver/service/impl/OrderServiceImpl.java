package org.trainingjava.orderserver.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.trainingjava.coreexception.NotFoundException;
import org.trainingjava.orderclient.event.OrderPaymentEvent;
import org.trainingjava.orderclient.event.OrderStatus;
import org.trainingjava.orderserver.dto.OrderRequest;
import org.trainingjava.orderserver.dto.OrderResponse;
import org.trainingjava.orderserver.entity.Order;
import org.trainingjava.orderserver.repository.OrderRepository;
import org.trainingjava.orderserver.service.OrderService;
import org.trainingjava.paymentclient.event.PaymentOrderEvent;

import java.util.Map;
import java.util.UUID;

@Slf4j
public class OrderServiceImpl implements OrderService {

    private final OrderRepository repository;

    private final StreamBridge streamBridge;

    private static final String ORDER_PAYMENT_SUPPLIER_BINDING_NAME = "orderPaymentSupplier-out-0";

    public OrderServiceImpl(OrderRepository repository, StreamBridge streamBridge) {
        this.repository = repository;
        this.streamBridge = streamBridge;
    }

    @Override
    public OrderResponse create(OrderRequest request) {
        log.info("(create)request : {}", request);
        Long totalPrice = countTotalPrice(request.items());
        Order order = Order.builder()
                .amount(totalPrice)
                .userId(request.userId())
                .status(OrderStatus.IN_PROGRESS)
                .build();
        order = repository.save(order);
        streamBridge
                .send(ORDER_PAYMENT_SUPPLIER_BINDING_NAME,
                        OrderPaymentEvent.builder()
                                .userId(order.getUserId())
                                .orderId(order.getId())
                                .amount(order.getAmount())
                                .orderStatus(order.getStatus())
                                .eventId(UUID.randomUUID().toString())
                                .build()
        );
        return OrderResponse.builder().orderId(order.getId())
                .userId(order.getUserId())
                .status(order.getStatus())
                .amount(order.getAmount())
                .build();
    }

    @Override
    public void handlePaymentEvent(PaymentOrderEvent event) {
        log.info("(handlePaymentEvent) event : {}", event);
        switch (event.getPaymentStatus()) {
            case SUCCESS -> {
                log.info("handle success case");
                Order order = repository.findById(event.getOrderId()).orElseThrow(NotFoundException::new);
                order.setStatus(OrderStatus.SUCCESS);
                repository.save(order);
            }
            case FAILED -> {
                log.info("handle failed case");
                Order order = repository.findById(event.getOrderId()).orElseThrow(NotFoundException::new);
                order.setStatus(OrderStatus.FAILED);
                repository.save(order);
            }
            case CANCELED -> {
                log.info("handle canceled case");
                Order order = repository.findById(event.getOrderId()).orElseThrow(NotFoundException::new);
                order.setStatus(OrderStatus.CANCELED);
                repository.save(order);
            }
        }
    }

    private Long countTotalPrice(Map<String, Long> items) {
        return items.values().stream().reduce(0L, Long::sum);
    }
}

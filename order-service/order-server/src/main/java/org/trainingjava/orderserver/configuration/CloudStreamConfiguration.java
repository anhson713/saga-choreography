package org.trainingjava.orderserver.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.trainingjava.orderserver.service.OrderService;
import org.trainingjava.paymentclient.event.PaymentOrderEvent;

import java.util.function.Consumer;

@Configuration
public class CloudStreamConfiguration {

    private final OrderService orderService;

    public CloudStreamConfiguration(OrderService orderService) {
        this.orderService = orderService;
    }

    @Bean
    public Consumer<PaymentOrderEvent> orderPaymentConsumer() {
        return orderService::handlePaymentEvent;
    }
}

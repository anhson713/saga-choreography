package org.trainingjava.orderserver.dto;

import lombok.Builder;
import org.trainingjava.orderclient.event.OrderStatus;

@Builder
public record OrderResponse(
    String orderId,
    String userId,
    long amount,
    OrderStatus status
) {}

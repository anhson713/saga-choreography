package org.trainingjava.orderserver.configuration;

import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.trainingjava.coreexceptionapi.configuration.EnableCoreExceptionApi;
import org.trainingjava.orderserver.repository.OrderRepository;
import org.trainingjava.orderserver.service.OrderService;
import org.trainingjava.orderserver.service.impl.OrderServiceImpl;

@Configuration
@EnableCoreExceptionApi
public class OrderConfiguration {


    @Bean
    public OrderService orderService(OrderRepository repository, StreamBridge streamBridge) {
        return new OrderServiceImpl(repository, streamBridge);
    }
}

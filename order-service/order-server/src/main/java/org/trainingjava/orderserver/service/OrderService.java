package org.trainingjava.orderserver.service;


import org.trainingjava.orderserver.dto.OrderRequest;
import org.trainingjava.orderserver.dto.OrderResponse;
import org.trainingjava.paymentclient.event.PaymentOrderEvent;

public interface OrderService {

    OrderResponse create(OrderRequest request);

    void handlePaymentEvent(PaymentOrderEvent event);
}

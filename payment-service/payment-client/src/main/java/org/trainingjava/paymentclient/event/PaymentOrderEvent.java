package org.trainingjava.paymentclient.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PaymentOrderEvent {

    private String eventId;
    private String userId;
    private String orderId;
    private PaymentStatus paymentStatus;
}

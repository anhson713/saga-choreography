package org.trainingjava.paymentclient.event;

public enum PaymentStatus {

    SUCCESS,
    FAILED,
    CANCELED
}

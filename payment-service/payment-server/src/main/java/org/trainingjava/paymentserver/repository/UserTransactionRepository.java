package org.trainingjava.paymentserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.trainingjava.paymentserver.entity.UserTransaction;

public interface UserTransactionRepository extends JpaRepository<UserTransaction, String> {
}

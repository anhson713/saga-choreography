package org.trainingjava.paymentserver.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trainingjava.paymentclient.event.PaymentStatus;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class UserTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    private String userId;

    private String orderId;

    @Enumerated(EnumType.STRING)
    private PaymentStatus paymentStatus;
}

package org.trainingjava.paymentserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.trainingjava.paymentserver.entity.UserBalance;

public interface UserBalanceRepository extends JpaRepository<UserBalance, String> {
}

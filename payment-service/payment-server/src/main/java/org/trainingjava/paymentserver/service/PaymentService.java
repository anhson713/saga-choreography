package org.trainingjava.paymentserver.service;

import org.trainingjava.orderclient.event.OrderPaymentEvent;
import org.trainingjava.paymentclient.event.PaymentOrderEvent;

public interface PaymentService {

    PaymentOrderEvent handleOrderEvent(OrderPaymentEvent event);
}

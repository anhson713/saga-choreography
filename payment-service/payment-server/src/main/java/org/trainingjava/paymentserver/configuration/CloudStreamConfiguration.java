package org.trainingjava.paymentserver.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.trainingjava.orderclient.event.OrderPaymentEvent;
import org.trainingjava.paymentclient.event.PaymentOrderEvent;
import org.trainingjava.paymentserver.service.PaymentService;

import java.util.function.Function;

@Configuration
public class CloudStreamConfiguration {

    private final PaymentService paymentService;

    public CloudStreamConfiguration(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Bean
    public Function<OrderPaymentEvent, PaymentOrderEvent> paymentProcessor() {
        return paymentService::handleOrderEvent;
    }
}

package org.trainingjava.paymentserver.service.impl;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.messaging.support.MessageBuilder;
import org.trainingjava.coreexception.NotFoundException;
import org.trainingjava.orderclient.event.OrderPaymentEvent;
import org.trainingjava.orderclient.event.OrderStatus;
import org.trainingjava.paymentclient.event.PaymentOrderEvent;
import org.trainingjava.paymentclient.event.PaymentStatus;
import org.trainingjava.paymentserver.entity.UserBalance;
import org.trainingjava.paymentserver.entity.UserTransaction;
import org.trainingjava.paymentserver.repository.UserBalanceRepository;
import org.trainingjava.paymentserver.repository.UserTransactionRepository;
import org.trainingjava.paymentserver.service.PaymentService;

import java.util.List;
import java.util.UUID;

@Slf4j
public class PaymentServiceImpl implements PaymentService {

    private final UserBalanceRepository userBalanceRepository;

    private final UserTransactionRepository userTransactionRepository;

    private final StreamBridge streamBridge;

    private static final String PAYMENT_ORDER_SUPPLIER_BINDING_NAME = "paymentOrderSupplier-out-0";

    public PaymentServiceImpl(UserBalanceRepository userBalanceRepository, UserTransactionRepository userTransactionRepository, StreamBridge streamBridge) {
        this.userBalanceRepository = userBalanceRepository;
        this.userTransactionRepository = userTransactionRepository;
        this.streamBridge = streamBridge;
    }

    @PostConstruct
    private void initUser() {
        List<UserBalance> userBalanceList = List.of(
                new UserBalance("1", 1000L),
                new UserBalance("2", 4000L),
                new UserBalance("3", 5000L)
        );
        userBalanceRepository.saveAll(userBalanceList);
    }

    @Override
    public PaymentOrderEvent handleOrderEvent(OrderPaymentEvent event) {
        log.info("(handleOrderEvent)event : {}", event);
        if (event.getOrderStatus().equals(OrderStatus.IN_PROGRESS)) {
            UserBalance userBalance = userBalanceRepository.findById(event.getUserId())
                    .orElseThrow(NotFoundException::new);
            if (userBalance.getBalance() > event.getAmount()) {
                log.info("handle success case");
                userBalance.setBalance(userBalance.getBalance() - event.getAmount());
                userBalanceRepository.save(userBalance);
                userTransactionRepository.save(UserTransaction
                        .builder()
                        .orderId(event.getOrderId())
                        .userId(event.getUserId())
                        .paymentStatus(PaymentStatus.SUCCESS).build());
                return PaymentOrderEvent.builder()
                        .orderId(event.getOrderId())
                        .userId(event.getUserId())
                        .eventId(UUID.randomUUID().toString())
                        .paymentStatus(PaymentStatus.SUCCESS).build();
            } else {
                log.info("handle failed case");
                userTransactionRepository.save(UserTransaction
                        .builder()
                        .orderId(event.getOrderId())
                        .userId(event.getUserId())
                        .paymentStatus(PaymentStatus.FAILED).build());
                return PaymentOrderEvent.builder()
                        .orderId(event.getOrderId())
                        .userId(event.getUserId())
                        .eventId(UUID.randomUUID().toString())
                        .paymentStatus(PaymentStatus.FAILED).build();
            }
        } else if (event.getOrderStatus().equals(OrderStatus.CANCELED)) {
            log.info("handle canceled case");
            UserBalance userBalance = userBalanceRepository.findById(event.getUserId())
                    .orElseThrow(NotFoundException::new);
            userBalance.setBalance(userBalance.getBalance() + event.getAmount());
            userBalanceRepository.save(userBalance);
            userTransactionRepository.save(UserTransaction
                    .builder()
                    .orderId(event.getOrderId())
                    .userId(event.getUserId())
                    .paymentStatus(PaymentStatus.CANCELED).build());
            return PaymentOrderEvent.builder()
                    .orderId(event.getOrderId())
                    .userId(event.getUserId())
                    .eventId(UUID.randomUUID().toString())
                    .paymentStatus(PaymentStatus.CANCELED).build();
        }
        return PaymentOrderEvent.builder().eventId(UUID.randomUUID().toString())
                .paymentStatus(PaymentStatus.FAILED).build();
    }
}

package org.trainingjava.paymentserver.configuration;

import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.trainingjava.paymentserver.repository.UserBalanceRepository;
import org.trainingjava.paymentserver.repository.UserTransactionRepository;
import org.trainingjava.paymentserver.service.PaymentService;
import org.trainingjava.paymentserver.service.impl.PaymentServiceImpl;

@Configuration
public class PaymentConfiguration {

    @Bean
    public PaymentService paymentService(UserBalanceRepository userBalanceRepository,
                                         UserTransactionRepository userTransactionRepository,
                                         StreamBridge streamBridge) {
        return new PaymentServiceImpl(userBalanceRepository, userTransactionRepository, streamBridge);
    }
}
